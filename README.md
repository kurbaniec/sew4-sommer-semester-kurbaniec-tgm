# "*sew4-sommer-semester-kurbaniec-tgm*"

## Design Patterns

* [Design Patterns Repository](https://bitbucket.org/kurbaniec/sew4-design-patterns-kurbaniec-tgm)
* [GK8 Strategy Calculator](https://bitbucket.org/kurbaniec/sew4-strategy-calculator-kurbaniec-tgm)
* [GK8 Strategy Duck-Simulator](https://bitbucket.org/kurbaniec/sew4-strategy-ducksim-kurbaniec-tgm)
* [GK8 Observer WeatherData](https://bitbucket.org/kurbaniec/sew4-observer-weather-kurbaniec-tgm)
* [GK8 Decorator TextReader](https://bitbucket.org/kurbaniec/sew4-decorator-textreader-kurbaniec-tgm)

## CRUD / Spring / Testing

* [GK8 SpringBoot Prime-Searcher](https://bitbucket.org/kurbaniec/sew4-prime-searcher-kurbaniec-tgm)
* [GK8 SpringBoot Userdata](https://bitbucket.org/kurbaniec/sew4-userdata-kurbaniec-tgm)
* [SoSe Testverbesserung](https://bitbucket.org/kurbaniec/sew4-sose-testverbesserung)
* [EK8 Group Chat](https://bitbucket.org/kurbaniec/sew4-group-chat-kurbaniec-tgm)